from utils import *
import os.path
import subprocess
from scipy.interpolate import interp1d
import sys
# import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'lmodern',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })

class cd:
  """Context manager for changing the current working directory"""
  def __init__(self, newPath):
      self.newPath = os.path.expanduser(newPath)

  def __enter__(self):
      self.savedPath = os.getcwd()
      os.chdir(self.newPath)

  def __exit__(self, etype, value, traceback):
      os.chdir(self.savedPath)

class Fit_EoS:
  """
      Fitting 1-, 2- and 3-argument equations of state with Pseudo-Polytrope formalism
  """

  ### Constructor
  def __init__(self, nb_arg, tablename, eosdir, nB_mid, nB_min, nB_max, gamma_low, kappa_low, dom_coef_Ye):
    """
      The parameters are the following:
      nb_arg         -- number of arguments of the EoS
      eosdir         -- Directory containing the original EoS
      nB_mid         -- Density of transition between external polytrope and intermediate Generalized Piecewise Polytrope
      nB_min         -- Minimal density down to which the fit is performed
      nB_max         -- Maximal density up to which the fit is performed
      gamma_low      -- Value of gamma set for low-density polytrope
      kappa_low      -- Value of kappa set for low-density polytrope in units of G=c=Msol=1
      kappa_low_cgs  -- Value of kappa set for low-density polytrope in cgs units (i.e. K*rho^gamma is in dyn/cm^2, where rho = mn*nb is in g/cm^3)
      kappa_low_lor  -- Value of kappa set for low-density polytrope in Lorene units (ie K*nb^gamma is in units of [rhonuc c^2] if nb is in units of 0.1 fm^-3)
      coefs_Ye       -- np.poly1d object that contains the K_i' coefficients (see the paragraph following Eq. (25) of PhysRevD.109.103022)
      dom_coef_Ye    -- Proportionality coefficient A (see the paragraph following Eq. (25) of PhysRevD.109.103022)
      table          -- Tuple containing the original EoS. Its elements are in the following order: 
                          neutron mass, proton mass, electron fraction, density, internal energy (without rest-mass),
                          pressure, free energy, baryon chemical potential, charge chemical potential,
                          lepton chemical potential, entropy, temperature, starting integer for nb, starting integer for theta, starting integer for ye
                        Some elements can be omitted depending on the nb_arg.
      e_frac         -- Electron fraction array
      dens           -- Baryons number density array
      dens_low       -- Baryons number density array, limited to values below nB_mid
      dens_mid       -- Baryons number density array, limited to values between nB_mid and nB_max
      dens_high      -- Baryons number density array, limited to values between nB_min and nB_max (fitting interval)
      coefs          -- List of fit coefficients
      fittable       -- Tuple containing the fitted thermo quantities. Its elements are in the following order:
                        density, electron fraction, pressure, internal energy, lepton chemical potential, neutron mass, proton mass, starting integer for theta, starting integer for nb, starting integer for ye
      fittable_beta  -- Tuple containing the beta-equilibrated version of the fitted table. Its elements are in the following order:
                        density, electron fraction, energy, pressure
      fittable_ortho -- Tuple containing the orthogonalised table (i.e. rectangular grid in (H, Y_e)). Its element are in the same order as in fittable.
    """
    self.nb_arg        = nb_arg
    self.tablename     = tablename
    self.eosdir        = eosdir
    self.nB_mid        = nB_mid
    self.nB_min        = nB_min
    self.nB_max        = nB_max
    self.gamma_low     = gamma_low
    self.kappa_low     = kappa_low
    self.kappa_low_cgs = kappa_low*msol_geom**(2.*(gamma_low-1.))/si_to_geom(gamma_low)/cgs_to_si(gamma_low)
    self.kappa_low_lor = kappa_low*msol_geom**(2.*(gamma_low-1.))/si_to_geom(gamma_low)/lor_to_si(gamma_low)
    self.coefs_Ye      = None
    self.dom_coef_Ye   = dom_coef_Ye
    self.table         = None
    self.e_frac        = None
    self.dens          = None
    self.dens_low      = None
    self.dens_mid      = None
    self.dens_high     = None
    self.coefs         = None
    self.fittable      = None
    self.fittable_beta = None
  
  ### Accessors
  def get_nb_arg(self):
    return self.nb_arg
  
  def get_tablename(self):
    return self.tablename
  
  def get_eosdir(self):
    return self.eosdir
  
  def get_nB_mid(self):
    return self.nB_mid
    
  def get_nB_min(self):
    return self.nB_min
  
  def get_nB_max(self):
    return self.nB_max
  
  def get_gamma_low(self):
    return self.gamma_low

  def get_kappa_low(self):
    return self.kappa_low

  def get_kappa_low_cgs(self):
    return self.kappa_low_cgs

  def get_kappa_low_lor(self):
    return self.kappa_low_lor
  
  def get_coefs_Ye(self):
    return self.coefs_Ye
  
  def get_dom_coef_Ye(self):
    return self.dom_coef_Ye

  def get_e_frac(self):
    return self.e_frac
  
  def get_dens(self):
    return self.dens
  
  def get_dens_low(self):
    return self.dens_low
  
  def get_dens_mid(self):
    return self.dens_mid
  
  def get_dens_high(self):
    return self.dens_high
  
  def get_coefs(self):
    return self.coefs
  
  def get_table(self):
    return self.table
  
  def get_fittable(self):
    return self.fittable
  
  def get_fittable_beta(self):
    return self.fittable_beta
  
  def get_fittable_ortho(self):
    return self.fittable_ortho
  
  ### Mutators
  def set_nb_arg(self, newval):
    # Not implemented: cannot be changed
    print("nb_arg cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_tablename(self, newval):
    # Not implemented: cannot be changed
    print("tablename cannot be changed ! Error. Aborting...")
    sys.exit()
    return None

  def set_eosdir(self, newval):
    # Not implemented: cannot be changed
    print("eosdir cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_nB_mid(self, newval):
    # Not implemented: cannot be changed
    print("nB_mid cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_nB_min(self, newval):
    # Not implemented: cannot be changed
    print("nB_min cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_nB_max(self, newval):
    # Not implemented: cannot be changed
    print("nB_max cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_gamma_low(self, newval):
    # Not implemented: cannot be changed
    print("gamma_low cannot be changed ! Error. Aborting...")
    sys.exit()
    return None
  
  def set_coefs_Ye(self, newval):
    self.coefs_Ye = newval
    return None

  def set_dom_coef_Ye(self, newval):
    self.dom_coef_Ye = newval
    return None
  
  def set_kappa_low(self, newval):
    self.kappa_low = newval
    self.kappa_low_cgs = newval*msol_geom**(2.*(self.get_gamma_low()-1.))/si_to_geom(self.get_gamma_low())/cgs_to_si(self.get_gamma_low())
    self.kappa_low_lor = newval*msol_geom**(2.*(self.get_gamma_low()-1.))/si_to_geom(self.get_gamma_low())/lor_to_si(self.get_gamma_low())
    return None
  
  def set_e_frac(self, newval):
    self.e_frac = newval
    return None
  
  def set_dens(self, newval):
    self.dens = newval
    return None
  
  def set_dens_low(self, newval):
    self.dens_low = newval
    return None
    
  def set_dens_mid(self, newval):
    self.dens_mid = newval
    return None
  
  def set_dens_high(self, newval):
    self.dens_high = newval
    return None
  
  def set_coefs(self, newval):
    self.coefs = newval
    return None
  
  def set_table(self, newval):
    self.table = newval
    return None
  
  def set_fittable(self, newval):
    self.fittable = newval
    return None
  
  def set_fittable_beta(self, newval):
    self.fittable_beta = newval
    return None
  
  def set_fittable_ortho(self, newval):
    self.fittable_ortho = newval
    return None

  ### Methods
  def read_table(self):
    """
        Reads the CompOSE table to be fitted
    """
    list_names = ["neutron mass", "proton mass", "electron fraction", "density", "internal energy",
                  "pressure", "free energy", "baryon chemical potential", "charge chemical potential",
                  "lepton chemical potential", "entropy", "temperature"," starting integer for nb", "starting integer for theta", "starting integer for ye"]
    if self.get_nb_arg() == 1:
      self.set_table(read_1D_table_compose(self.get_eosdir()))
      self.set_dens(self.get_table()[2])
      condition      = (self.get_dens() >= self.get_nB_min()) & (self.get_dens() <= self.get_nB_max())
      condition_mid  = (self.get_dens() >= self.get_nB_mid()) & (self.get_dens() < self.get_nB_min())
      condition_low  = (self.get_dens() < self.get_nB_mid())
      self.set_dens_low(self.get_dens()[condition_low])
      self.set_dens_mid(self.get_dens()[condition_mid])
      self.set_dens_high(self.get_dens()[condition])
    elif self.get_nb_arg() == 2:
      self.set_table({name:array for name,array in zip(list_names, read_2D_table_compose(self.get_eosdir()))})
      self.set_e_frac(self.get_table()["electron fraction"])
      self.set_dens(self.get_table()["density"])
      condition      = (self.get_dens() >= self.get_nB_min()) & (self.get_dens() <= self.get_nB_max())
      condition_mid  = (self.get_dens() >= self.get_nB_mid()) & (self.get_dens() < self.get_nB_min())
      condition_low  = (self.get_dens() < self.get_nB_mid())
      self.set_dens_low(self.get_dens()[condition_low])
      self.set_dens_mid(self.get_dens()[condition_mid])
      self.set_dens_high(self.get_dens()[condition])
    else:
      print("Cases of 3 and more args EoS not implemented yet ! Aborting.")
      sys.exit()
    
    return None
  
  def fit_1D(self, initial_guess, method):
    assert(self.get_nb_arg() == 1)
    # Unpacking table
    m_n,m_p,n_B,eps,press,free,mu_B,mu_q,mu_l = self.get_table()

    # Parameter lists
    condition = (n_B >= self.get_nB_min()) & (n_B <= self.get_nB_max())
    Ft     = free[condition]
    pret   = press[condition]
    lognbt = np.log(self.get_dens_high())

    # Data to fit on
    gam1 = np.gradient(np.log(pret),lognbt)  # adiabatic index
    dFdx = pret/(m_n*self.get_dens_high())         # p/n = n*df/dn= df/d(log(n))

    print("Fitting internal energy...")
    start = timer()
    self.set_coefs(fit_func_1D(self.get_dens_high(), Ft, gam1, dFdx, initial_guess, method, self))
    end   = timer()
    print("... done.")
    print(f"Total time spent fitting: {end-start:.2f} [s]")
    
    return None
  
  def compute_thermo_1D(self):
    """
        Computes pressure and energy in cgs units for 1D pseudo-polytrope fitting.
    """
    assert(self.get_nb_arg() == 1)
    assert(self.get_coefs() is not None)

    # Unpacking table and coefs
    m_n,m_p,n_B,eps,press,free,mu_B,mu_q,mu_l = self.get_table()
    coefs                      = self.get_coefs()
    coefs_poly = coefs[:-1]
    alpha      = coefs[-1]

    # Computing GPP parameters
    n_lim1 = self.get_nB_mid()
    n_lim2 = self.get_nB_min()
    gamma_low = self.get_gamma_low()
    Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda = compute_GPP_param(self, *coefs, verbose = True)


    condition      = (self.get_dens() >= n_lim2) & (self.get_dens() <= self.get_nB_max())
    condition_low  = (self.get_dens() < n_lim1)
    condition_mid  = (self.get_dens() >= n_lim1) & (self.get_dens() < n_lim2)
    
    self.set_dens_low(self.get_dens()[condition_low])
    self.set_dens_mid(self.get_dens()[condition_mid])
    self.set_dens_high(self.get_dens()[condition])


    # Utilities
    nbpts_crust    = len(self.get_dens_low())
    nbpts_mid      = len(self.get_dens_mid())
    nbpts_fit      = len(self.get_dens_high())
    nbpts          = nbpts_crust + nbpts_fit + nbpts_mid

    # Target arrays
    ener_cgs  = np.zeros(nbpts)
    press_cgs = np.zeros(nbpts)
    eps_fit   = np.zeros(nbpts)
    ent_fit   = np.zeros(nbpts)

    # Filling the arrays
    for i,nb in enumerate(self.get_dens_high()):
      index_high = nbpts_crust + nbpts_mid + i
      xxx  = np.log(nb)
      fit  = func_1D(xxx, *coefs_poly, alpha, ddd, Lambda)
      dfit = dfunc_1D(xxx, *coefs_poly, alpha, ddd, Lambda)

      press_cgs[index_high] = nb*dfit*m0_MeV*p_convert
      ener_cgs[index_high]  = nb*m0_MeV*eps_convert*(fit+1.)
      eps_fit[index_high]   = fit
      ent_fit[index_high]   = np.log1p(fit+dfit)
  
    
    for i,nb in enumerate(self.get_dens_mid()):
      index_mid = nbpts_crust + i
      press_cgs[index_mid] = Kap_GPP*(m0_MeV*nb*eps_convert)**Gam_GPP + Lambda_GPP*m0_MeV*p_convert
      ener_cgs[index_mid]  = Kap_GPP*(m0_MeV*nb*eps_convert)**Gam_GPP/(Gam_GPP-1.)/c2_cgs + (1.+d_GPP)*nb*m0_MeV*eps_convert - Lambda_GPP*m0_MeV*eps_convert
      eps_fit[index_mid]   = Kap_GPP*(m0_MeV*nb*eps_convert)**(Gam_GPP-1.)/(Gam_GPP-1.)/c2_cgs + d_GPP - Lambda_GPP/nb
      ent_fit[index_mid]   = np.log1p(press_cgs[index_mid]/m0_MeV/p_convert/nb + eps_fit[index_mid])
    
    kappa_low_lor = self.get_kappa_low_lor()    
    for i,nb in enumerate(self.get_dens_low()):
      press_low = kappa_low_lor * (10.*nb)**gamma_low

      press_cgs[i] = press_low * rhonuc_cgs*c2_cgs
      ener_cgs[i]  = kappa_low_lor*(10.*nb)**gamma_low/(gamma_low-1.) * rhonuc_cgs + nb*m0_MeV*eps_convert
      eps_fit[i]   = kappa_low_lor*(10.*nb)**(gamma_low-1.)/(gamma_low-1.)
      ent_fit[i]   = np.log1p(press_low/nb/m0_MeV + eps_fit[i])
    
    self.set_fittable((m_n, m_p, n_B, eps_fit, press_cgs/p_convert, ent_fit))

    return press_cgs,ener_cgs
  
  def fit_2D(self, initial_guess, method):
    # Unpacking the table
    m_n,m_p,Y_e,n_B,eps,press,free,mu_B,mu_q,mu_l,entropy,T,int_nb_0,int_t_0,int_ye_0 = list(self.get_table().values())

    # Parameter lists and number of points for core/crust
    nB_lim    = self.get_nB_min()
    x_lim     = np.log(nB_lim)
    condition = (n_B >= nB_lim) & (n_B <= self.get_nB_max())
    fit_Ye    = self.get_e_frac()

    # Data to fit on
    fit_eps   = eps[condition].T
    fit_press = press[condition].T
    fit_gam1  = np.zeros(np.shape(fit_eps))
    fit_dFdx  = np.zeros(np.shape(fit_eps))
    nbt       = n_B[condition]
    lognbt    = np.log(nbt)
    for i,ye in enumerate(fit_Ye):
      fit_gam1[i, :] = np.gradient(np.log(fit_press[i, :]), lognbt, edge_order = 2)
      fit_dFdx[i, :] = fit_press[i, :]/(m_n*nbt)
    
    kappa_min = fit_eps[:, 0]
    param_poly = np.polyfit(Y_e[:], kappa_min, 16)
    print(param_poly)
    poly = np.poly1d(param_poly)
    self.coefs_Ye = poly

    print("Fitting internal energy...")
    start = timer()
    popt, param_GPP  = fit_func_2D(self.get_dens_high(), fit_Ye, fit_eps, fit_gam1, fit_dFdx, initial_guess, self, method)
    end   = timer()
    print("... done.")
    print(f"Total time spent fitting: {end-start:.2f} [s]")

    self.set_coefs((popt, param_GPP))

    return None
  
  def perform_fit(self, nb_coef, method = 'trf'):
    """
        Performs the fit of the table according to the pseudo-polytrope formalism, using the desired minimal square method (default is Trust Reflective Algorithm, a.k.a. 'trf')
    """
    assert(self.get_table() is not None)

    initial_guess = [2*10**(-i) for i in range(nb_coef)] + [1.5]
    
    if self.get_nb_arg() == 1:
      self.fit_1D(initial_guess, method)
    elif self.get_nb_arg() == 2:
      self.fit_2D(initial_guess, method)

    return None
  

  def write_lorene_1D(self, filename, header, n_B, ener_cgs, press_cgs):
    """
        Write to standard Lorene format. /!\ Pressure and energy must be in CGS units /!\
    """
    lines_1D = list()
    for index,nb in enumerate(n_B):
      lines_1D.append("{0}\t{1:.16e}\t{2:.16e}\t{3:.16e}\t".format(index,nb,ener_cgs[index],press_cgs[index]))
    
    with open(self.get_eosdir() + filename + ".d","w") as file_1D:
      #Writing header of file
      empty = "# "+'\n'
      header_sentence = header + '\n'
      nbr_of_lines = str(len(n_B)) + " <-- Number of lines" + '\n'
      physical_quantities = "#" + '\t' +  "n_B [1/fm^3]" +  '\t' +  "e [g/cm^3]" + '\t' + "P [dyn/cm^2]" + '\n'
      file_1D.write(empty+empty+header_sentence+empty+empty+nbr_of_lines+empty+physical_quantities+empty)

      file_1D.writelines("\n".join(lines_1D))
    
    return None

  def write_lorene_2D(self):
    """
        Write to extended Lorene format. This 2D format is intended for density and electron fraction as the two arguments.
        Please provide the quantities in CGS units.
    """
    assert(self.get_nb_arg() == 2)
    
    # Unpacking the table
    m_n,m_p,Y_e,n_B,eps,press,free,mu_B,mu_q,mu_l,entropy,T,int_nb_0,int_t_0,int_ye_0 = list(self.get_table().values())
    nbpts_tot = len(self.get_dens())
    nbpts_Ye  = len(Y_e)

    # Thermo fit lists
    press_fit = np.zeros((nbpts_Ye,nbpts_tot))
    ener_fit  = np.zeros((nbpts_Ye,nbpts_tot))
    eps_fit   = np.zeros((nbpts_Ye,nbpts_tot))
    ent_fit   = np.zeros((nbpts_Ye,nbpts_tot))

    # Density arrays for core and crust
    nbpts_crust   = len(self.get_dens_low())
    nbpts_mid     = len(self.get_dens_mid())

    # Unpacking the coefficients
    coefs, coefs_GPP = self.get_coefs()
    Gam_GPP    = coefs_GPP[:, 0]
    Kap_GPP    = coefs_GPP[:, 1]
    d_GPP      = coefs_GPP[:, 2]
    Lambda_GPP = coefs_GPP[:, 3]
    # kappa_low_cgs    = self.get_kappa_low_cgs()
    gamma_low        = self.get_gamma_low()
    
    Kappa_Ye = self.get_coefs_Ye()
    AAA = self.get_dom_coef_Ye()

    ### Write header
    self.write_header()
    

    
    print("Creating the lower density part...")
    start = timer()
    for j, ye in enumerate(Y_e):
      new_kappa = AAA*Kappa_Ye(ye)
      self.set_kappa_low(new_kappa)
      kappa_low_cgs = self.get_kappa_low_cgs()
      for index,nb in enumerate(self.get_dens_low()):
        ## 2D table
        press_low = kappa_low_cgs*(nb*m0_MeV*eps_convert)**gamma_low
        ener_low  = kappa_low_cgs*(nb*m0_MeV*eps_convert)**gamma_low/(gamma_low-1.)/c2_cgs + nb*m0_MeV*eps_convert
        eps_low   = kappa_low_cgs*(nb*m0_MeV*eps_convert)**(gamma_low-1.)/(gamma_low-1.)/c2_cgs
        ent_low   = np.log1p(eps_low + press_low/(nb*m0_MeV*p_convert))
        press_fit[j, index] = press_low
        ener_fit[j, index]  = ener_low
        eps_fit[j, index]   = eps_low
        ent_fit[j, index]   = ent_low
      

    print("... done.")
    # self.set_kappa_low(kappa_ini)
    
    print("Creating the mid density part...")
    for index,nb in enumerate(self.get_dens_mid()):
      index_mid = nbpts_crust + index
      press_mid = Kap_GPP*(nb*m0_MeV*eps_convert)**Gam_GPP + Lambda_GPP*m0_MeV*p_convert
      ener_mid  = Kap_GPP*(nb*m0_MeV*eps_convert)**Gam_GPP/(Gam_GPP-1.)/c2_cgs + (1.+d_GPP)*nb*m0_MeV*eps_convert - Lambda_GPP*m0_MeV*eps_convert
      eps_mid   = Kap_GPP*(nb*m0_MeV*eps_convert)**(Gam_GPP-1.)/(Gam_GPP-1.)/c2_cgs + d_GPP - Lambda_GPP/nb
      ent_mid   = np.log1p(eps_mid + press_mid/(nb*m0_MeV*p_convert))
      press_fit[:, index_mid] = press_mid
      ener_fit[:, index_mid]  = ener_mid
      eps_fit[:, index_mid]   = eps_mid
      ent_fit[:, index_mid]   = ent_mid
    print("... done.")
    
    print("Creating the higher density part...")
    for index, nb in enumerate(self.get_dens_high()):
      index_high = nbpts_crust + nbpts_mid + index
      x = np.log(nb)
      for i_Y, ye in enumerate(self.get_e_frac()):
        fit   = func_1D(x, *coefs[i_Y, :])
        dxfit = dfunc_1D(x, *coefs[i_Y, :])
        press_high_cgs = nb*m0_MeV*dxfit*p_convert
        ener_high_cgs  = nb*m0_MeV*eps_convert*(fit+1.)
        ent_high       = np.log1p(fit + dxfit)
        press_fit[i_Y, index_high] = press_high_cgs
        ener_fit[i_Y, index_high]  = ener_high_cgs
        eps_fit[i_Y, index_high]   = fit
        ent_fit[i_Y, index_high]   = ent_high
    end = timer()
    print("... done.")
    print(f"Total time spent creating data: {end-start:.2f} [s]")

    print("Computing derivatives...")
    start = timer()
    cs2_tbl   = np.zeros((nbpts_Ye,nbpts_tot))
    mu_l_tbl  = np.zeros((nbpts_Ye,nbpts_tot))
    d2p_tbl   = press_fit + ener_fit*c2_cgs
    for i,ye in enumerate(Y_e):
      cs2_tbl[i,:]   = np.gradient(press_fit[i,:],ener_fit[i,:],edge_order=2)/c2_cgs        # [c^2]
    for index,nb in enumerate(n_B):
      mu_l_tbl[:,index]  = np.gradient(eps_fit[:,index],Y_e,edge_order=2)               # [m_n]
      d2p_tbl[:,index]   = np.gradient(d2p_tbl[:,index],Y_e,edge_order=2)
    end = timer()
    print("... done.")
    print(f"Total time spent computing derivatives: {end-start:.2f} [s]")


    ### Formatting and writing to file
    print("Formatting...") 
    lines_ = list() # List containing the formatted data to write
    start = timer()   
    for i,ye in enumerate(Y_e):
      first_call = 1
      for index,nb in enumerate(self.get_dens_low()):
        if first_call:
          ww = ent_fit[i,index]
          first_call = 0
        ent = ent_fit[i,index] #- ww + 1e-14
        # if ent < 0:
        #     print("WARNING ! NEGATIVE ENTHALPY AFTER RESCALING")
        lines_.append("{0}\t{1:.16e}\t{2:.16e}\t{3:.16e}\t{4:.16e}\t{5:.16e}\t{6:.16e}\t{7:.16e}\t{8:.16e}\t{9:.16e}".format(index,nb,ener_fit[i,index],press_fit[i,index],ent,ye,cs2_tbl[i,index],mu_l_tbl[i,index]*m_n,d2p_tbl[i,index],0.))
      for index,nb in enumerate(self.get_dens_mid()):
        index_mid = nbpts_crust + index
        ent = ent_fit[i, index_mid] #- ww + 1e-14
        lines_.append("{0}\t{1:.16e}\t{2:.16e}\t{3:.16e}\t{4:.16e}\t{5:.16e}\t{6:.16e}\t{7:.16e}\t{8:.16e}\t{9:.16e}".format(index_mid,nb,ener_fit[i,index_mid],press_fit[i,index_mid],ent,ye,cs2_tbl[i,index_mid],mu_l_tbl[i,index_mid]*m_n,d2p_tbl[i,index_mid],0.))
      for index,nb in enumerate(self.get_dens_high()):
        index_high = nbpts_crust + nbpts_mid + index
        ent = ent_fit[i,index_high] #- ww + 1e-14
        # if ent < 0:
        #     print("WARNING ! NEGATIVE ENTHALPY AFTER RESCALING")
        lines_.append("{0}\t{1:.16e}\t{2:.16e}\t{3:.16e}\t{4:.16e}\t{5:.16e}\t{6:.16e}\t{7:.16e}\t{8:.16e}\t{9:.16e}".format(index_high,nb,ener_fit[i,index_high],press_fit[i,index_high],ent,ye,cs2_tbl[i,index_high],mu_l_tbl[i,index_high]*m_n,d2p_tbl[i,index_high],0.))
    end = timer()
    print("... done.")
    print(f"Total time spent formatting: {end-start:.2f} [s]")
    list_names = ["density", "electron fraction", "pressure", "internal energy", "lepton chemical potential", "neutron mass", "proton mass", "starting integer for theta", "starting integer for nb", "starting integer for ye"]
    self.set_fittable({name:array for name,array in zip(list_names, (n_B, Y_e, press_fit/p_convert, eps_fit, mu_l_tbl, m_n, m_p, int_t_0, int_nb_0, int_ye_0))})

    #Appending to file
    print("Writing data to file...")
    start = timer()
    print("{} lines to write.".format(len(lines_)))
    with open(self.get_eosdir() + f"fit/{self.get_tablename()}.d","a") as filefit:
      filefit.writelines("\n".join(lines_))
    end = timer()
    print("... done.")
    print(f"Total time spent writing data on file: {end-start:.2f} [s]")

    return None

  def write_compose(self):
    """
        Writes data to eos.thermo according to the CompOSE format.
    """
    #Unpacking fitted table
    n_B, Y_e, press_fit, eps_fit, mu_l_tbl, m_n, m_p, int_t_0, int_nb_0, int_ye_0 = list(self.get_fittable().values())
    print("Writing thermo fit quantities in CompOSE format...")
    start = timer()  
    write_compose_format(self.get_eosdir() + "fit/", n_B, Y_e, press_fit*p_convert, eps_fit, mu_l_tbl, m_n, m_p, int_t_0, int_nb_0, int_ye_0)
    end = timer()
    print("... done.")
    print(f"Total time spent writing in CompOSE format: {end-start:.2f} [s]")
    return None


  def execute_compose_code(self, *args):
    """
        Calls the ./compose executable of CompOSE code, allowing to perform diverse actions on the thermo data such as beta-eq  (see the CompOSE website https://compose.obspm.fr)
    """
    
    if not args:
      with cd(self.get_eosdir() + "fit"):
        print("Please type your input: 1, 2 or 3.")
        EXIT_SUCCESS = subprocess.run(self.get_eosdir() + "fit/compose", check = True)
    else:
      with cd(self.get_eosdir() + "fit"):
        cmd_line = f"yes {args[0]} | " + self.get_eosdir() + "fit/compose"
        EXIT_SUCCESS = subprocess.run(cmd_line, shell=True, check=True)
    
    return None

  def compute_beta_eq(self):
    """
        Calls the CompOSE code to compute beta equilibrium from the fit of the 2D table
    """
    ### Ensuring the appropriate files are in the directory
    assert(os.path.isfile(self.get_eosdir() + "fit/eos.thermo"))
    assert(os.path.isfile(self.get_eosdir() + "fit/eos.parameters"))
    assert(os.path.isfile(self.get_eosdir() + "fit/eos.quantities"))
    assert(os.path.isfile(self.get_eosdir() + "fit/compose"))

    self.execute_compose_code(3)
    
    return None
  
  def write_beta_eq_files(self):
    """
        Writes beta-eq data to a 1D Lorene standard format as well as ye_beta.d file.
    """
    assert(os.path.isfile(self.get_eosdir() + "fit/eos.beta"))

    self.set_fittable_beta(read_1D_table_beta(self.get_eosdir() + "fit/"))
    n_B_1D,Y_e_beta,ener_1D,press_1D = self.get_fittable_beta()
    m_n = self.get_table()["neutron mass"]

    ener_cgs  = (ener_1D*eps_convert)[n_B_1D < self.get_nB_max()]
    press_cgs = (press_1D*p_convert)[n_B_1D < self.get_nB_max()]

    filename = f"fit/{self.get_tablename()}_beta"
    header   = "# EoS created from beta-eq computation of fitted EoS created from two-argument EoS. Argument is n_B"

    print("Writing data to beta files...")
    start = timer()

    self.write_lorene_1D(filename, header, n_B_1D[n_B_1D < self.get_nB_max()], ener_cgs, press_cgs)

    ### Writing ye_beta.d file
    ent_beta_1D = np.log((ener_1D + press_1D)/(n_B_1D*m_n))[n_B_1D < self.get_nB_max()]
    with open(self.get_eosdir() + "fit/ye_beta.d","w") as file_ye_beta:
      first_call = 1
      for ent,ye in zip(ent_beta_1D,Y_e_beta):
        if first_call:
          ww = ent
          first_call = 0
        ent = ent - ww + 1e-14
        file_ye_beta.write("{0:.16e}\t{1:.16e}\n".format(ent,ye))
    
    end = timer()
    print("... done.")
    print(f"Total time spent writing data on file: {end-start:.2f} [s]")
    
    return None


  # def plot_thermo(self, thermo_var, axis, index, fig_height = 15, fig_width = 15):
  #   """
  #       Plots the thermodynamic variable located at the index thermo_var, in the direction axis (0 = density, 1 = electron fraction), at the index index
  #       regarding the fixed direction.
  #   """
  #   n_B       = self.get_dens()
  #   Y_e       = self.get_e_frac() if self.get_nb_arg() == 2 else [r"beta equilibrium"]
  #   vars      = (n_B,Y_e)
  #   vars_name = ["n_B","Y_e"]

  #   thermo_eos = (self.get_table()[thermo_var]).take(index, axis = (axis + 1) % 2) if self.get_nb_arg() == 2 else self.get_table()[thermo_var]
  #   thermo_fit = (self.get_fittable()[thermo_var]).take(index, axis = axis) if self.get_nb_arg() == 2 else self.get_fittable()[thermo_var]

  #   fig = plt.figure(figsize=[cm_to_inch(2.*fig_width),cm_to_inch(fig_height)])
  #   ax1 = fig.add_subplot(1,2,1)
  #   ax1.plot(vars[axis], thermo_eos, label = f"EoS at ${vars_name[(axis + 1) % 2]}={(vars[(axis + 1) % 2])[index]}$", color='k')
  #   if axis == 0:
  #     ax1.plot((vars[axis])[n_B < self.get_nB_min()], thermo_fit[n_B < self.get_nB_min()], label = f"Fit at ${vars_name[(axis + 1) % 2]}={(vars[(axis + 1) % 2])[index]}$", color='r', linestyle = "dashed")
  #     ax1.plot((vars[axis])[n_B > self.get_nB_min()], thermo_fit[n_B > self.get_nB_min()], label = f"Fit at ${vars_name[(axis + 1) % 2]}={(vars[(axis + 1) % 2])[index]}$", color='g', linestyle = "dashed")
  #   else:
  #     ax1.plot((vars[axis]), thermo_fit, label = f"Fit at ${vars_name[(axis + 1) % 2]}={(vars[(axis + 1) % 2])[index]}$", color='r', linestyle = "dashed")
  #   plt.legend()
  #   plt.grid()

  #   ax2 = fig.add_subplot(1,2,2)
  #   ax2.plot(vars[axis], np.abs((thermo_eos-thermo_fit)/thermo_eos), label = f"Relative difference of {thermo_var} between EoS and fit", color = 'k')

  #   if (axis == 0):
  #     ax1.set_xscale("log")
  #   ax1.set_yscale("log")

  #   ax2.set_xscale("log")
  #   ax2.set_yscale("log")

  #   plt.legend()
  #   plt.grid()

  #   plt.tight_layout()
  #   plt.show()
      
  def write_header(self, tablename_suffix = ""):
    with open(self.get_eosdir() + f"fit/{self.get_tablename()}{tablename_suffix}.d","w") as filefit:
      #Writing header of file
      empty = "# "+'\n'
      header_sentence = "# EoS created from fit of two-argument EoS. Arguments are n_B and Y_e" + '\n'
      nbr_of_lines = str(len(self.get_dens_low()) + len(self.get_dens_mid()) + len(self.get_dens_high())) + '\t' + str(len(self.get_e_frac())) + " points in n_B and Y_e" + '\n'
      physical_quantities = "#" + '\t' +  "n_B [1/fm^3]" +  '\t' +  "e [g/cm^3]" + '\t' + "P [dyn/cm^2]" + '\t' + "H [c^2]" + '\t' + "Y_e [dimensionless]" + '\t' + "c_s^2 [c^2]" \
                            + '\t' + "mu_e [MeV]" + '\t' + "d2p/dHdYe [dyn c^-2 cm^-2]" + '\n'
      filefit.write(empty+empty+header_sentence+empty+empty+nbr_of_lines+empty+physical_quantities+empty)
        
  def orthogonalise_table(self):
    ### Read data and unpack coefs
    nB, ener, press, entha, Ye, cs2, mue, d2p = read_2D_table_lorene(self.get_eosdir())
    coefs, coefs_GPP = self.get_coefs()
    Gam_GPP    = coefs_GPP[:, 0]
    Kap_GPP    = coefs_GPP[:, 1]
    d_GPP      = coefs_GPP[:, 2]
    Lambda_GPP = coefs_GPP[:, 3]

    gamma_low = self.get_gamma_low()
    Kappa_Ye  = self.get_coefs_Ye()
    AAA = self.get_dom_coef_Ye()
       
    ### Retrieving original data of fitted table
    n_B_raw, Y_e_raw, press_fit_raw, eps_fit_raw, mu_l_tbl_raw, m_n, m_p, int_t_0, int_nb_0, int_ye_0 = list(self.get_fittable().values())
    
    
    ### Compute physical quantities
    def compute_press(nb, index_ye):
      if nb < self.get_nB_mid():
        ye = Ye[0, index_ye]
        new_kappa = AAA*Kappa_Ye(ye)
        self.set_kappa_low(new_kappa)
        kappa_low_cgs = self.get_kappa_low_cgs()
        return kappa_low_cgs*(nb*m0_MeV*eps_convert)**gamma_low/p_convert/m0_MeV
      elif self.get_nB_mid() <= nb < self.get_nB_min():
        return Kap_GPP[index_ye]*(nb*m0_MeV*eps_convert)**Gam_GPP[index_ye]/m0_MeV/p_convert + Lambda_GPP[index_ye]
      else:
        dxfit = dfunc_1D(np.log(nb), *coefs[index_ye, :])
        return nb*dxfit
    def compute_eps(nb, index_ye):
      if nb < self.get_nB_mid():
        ye = Ye[0, index_ye]
        new_kappa = AAA*Kappa_Ye(ye)
        self.set_kappa_low(new_kappa)
        kappa_low_cgs = self.get_kappa_low_cgs()
        return kappa_low_cgs*(nb*m0_MeV*eps_convert)**(gamma_low-1.)/(gamma_low-1.)/c2_cgs
      elif self.get_nB_mid() <= nb < self.get_nB_min():
        return Kap_GPP[index_ye]*(nb*m0_MeV*eps_convert)**(Gam_GPP[index_ye]-1.)/(Gam_GPP[index_ye]-1.)/c2_cgs + d_GPP[index_ye] - Lambda_GPP[index_ye]/nb
      else:
        return func_1D(np.log(nb), *coefs[index_ye, :])
    def compute_entha(nb, index_ye, H0):
      if nb < self.get_nB_mid():
        ye = Ye[0, index_ye]
        new_kappa = AAA*Kappa_Ye(ye)
        self.set_kappa_low(new_kappa)
        kappa_low_cgs = self.get_kappa_low_cgs()
        press_low = kappa_low_cgs*(nb*m0_MeV*eps_convert)**gamma_low
        eps_low   = kappa_low_cgs*(nb*m0_MeV*eps_convert)**(gamma_low-1.)/(gamma_low-1.)/c2_cgs
        return np.log1p(eps_low + press_low/(nb*m0_MeV*p_convert)) - H0
      elif self.get_nB_mid() <= nb < self.get_nB_min():
        press_mid = Kap_GPP[index_ye]*(nb*m0_MeV*eps_convert)**Gam_GPP[index_ye] + Lambda_GPP[index_ye]*m0_MeV*p_convert
        eps_mid   = Kap_GPP[index_ye]*(nb*m0_MeV*eps_convert)**(Gam_GPP[index_ye]-1.)/(Gam_GPP[index_ye]-1.)/c2_cgs + d_GPP[index_ye] - Lambda_GPP[index_ye]/nb
        ent_mid   = np.log1p(eps_mid + press_mid/(nb*m0_MeV*p_convert))
        return ent_mid
      else:
        x     = np.log(nb)
        fit   = func_1D(x, *coefs[index_ye, :])
        dxfit = dfunc_1D(x, *coefs[index_ye, :])
        return np.log((1+fit+dxfit)) - H0
    
    ent_ref = entha[:, 0] - entha[0,0]
    nbpts_Ye = len(Ye[0])
    nbpts_nb = len(ent_ref)
    IterMax = 30
    tol = 5e-16
    
    nb_ortho = np.zeros((nbpts_nb, nbpts_Ye))
    nb_ortho[:, 0] = nB[:, 0]
    nb_ortho[0, :] = nB[0, 0]
    
    first_output = 0
    print("Starting orthogonalisation... ")
    start = timer()
    for i_Ye in range(1, nbpts_Ye):
      ye = Ye[0, i_Ye]
      H0 = entha[0, i_Ye]
      for j in range(1, nbpts_nb):
        rate = np.floor(((i_Ye-1)*(nbpts_nb-1) + j-1)/((nbpts_nb-1)*(nbpts_Ye-1)) * 100)
        if rate % 5 == 0 and first_output == 0:
          print("=======================================================")
          print(f"Indices: {j}/{nbpts_nb-1} (n_B); {i_Ye}/{nbpts_Ye-1} (Ye). {rate:.0f}%")
          first_output = 1
        if rate % 5 == 1:
          first_output = 0
        ent0 = entha[j, i_Ye]
        ent_resc1 = ent0 - H0
        ent_resc2 = ent_ref[j]
        ind_ent = find_closest(ent_ref, ent_resc1)
        k=0
        f = lambda nb, index_ye, H0: compute_entha(nb, index_ye, H0) - ent_resc2
        AA = nB[ind_ent, i_Ye]
        BB = nB[ind_ent, i_Ye]
        while f(AA, i_Ye, H0)*f(BB, i_Ye, H0)>0:
            k += 1
            if f(BB, i_Ye, H0)>0: 
              if ind_ent-k >= 0 :
                AA = nB[ind_ent-k, i_Ye]
              else:
                AA = nB[0, i_Ye]/k
            elif f(AA, i_Ye, H0)<0:
              if ind_ent+k < nbpts_nb:
                BB = nB[ind_ent+k, i_Ye]
              else:
                BB = nB[nbpts_nb-1, i_Ye] + k*1e-2    
        
        nb_p = regulafalsi(f, AA, BB, ent_resc2, IterMax, tol, i_Ye, H0)
        if f(AA, i_Ye, H0) == 0:
          nb_p = AA
        elif f(BB, i_Ye, H0) == 0:
          nb_p = BB
        nb_ortho[j, i_Ye] = nb_p
    end = timer()
    print("... done.")
    print(f"Total time spent orthogonalising: {end-start:.2f} [s]")

    ### Write header of orthogonalised file
    tablename_suffix = "_ortho"
    self.write_header(tablename_suffix)
    
    # Thermo fit lists
    press_fit = np.zeros((nbpts_Ye,nbpts_nb))
    ener_fit  = np.zeros((nbpts_Ye,nbpts_nb))
    eps_fit   = np.zeros((nbpts_Ye,nbpts_nb))
    ent_fit   = np.zeros((nbpts_Ye,nbpts_nb))
    
    print("Creating the orthogonalised table data... ")
    start = timer()
    for j, ye in enumerate(Ye[0]):
      # mu0m1 = interp_mu0(ye)
      for index, nb in enumerate(nb_ortho[:, j]):
        ## 2D table
        p0     = compute_press(nb, j)*m0_MeV                           # []
        p_cgs  = p0 * c2_cgs * eps_convert                                            # [dyn cm^-2]
        eps0   = compute_eps(nb, j)        # []
        ener   = (eps0 + 1.) * nb * m0_MeV
        ent    = compute_entha(nb, j, 0.)
        press_fit[j, index] = p_cgs
        ener_fit[j, index]  = ener*eps_convert
        ent_fit[j, index]   = ent
        eps_fit[j, index]   = eps0
    end = timer()
    print("... done.")
    print(f"Total time spent creating data: {end-start:.2f} [s]")

    print("Computing derivatives...")
    start = timer()
    cs2_tbl   = np.zeros((nbpts_Ye, nbpts_nb))
    mu_l_tbl  = np.zeros((nbpts_Ye, nbpts_nb))
    d2p_tbl   = press_fit + ener_fit*c2_cgs
    d2p_tbl_regridded  = np.zeros((nbpts_Ye, nbpts_nb))
    mu_l_tbl_regridded = np.zeros((nbpts_Ye, nbpts_nb))
    eps_regridded      = np.zeros((nbpts_Ye, nbpts_nb))
    nB_const = np.logspace(np.log10(nb_ortho[0, 0]), np.log10(nb_ortho[-1, 0]), len(nb_ortho))
    for i,ye in enumerate(Ye[0]):
      cs2_tbl[i,:]   = np.gradient(press_fit[i, :],ener_fit[i, :],edge_order=2)/c2_cgs        # [c^2]
      
      fint = interp1d(nb_ortho[:, i], eps_fit[i, :], 'cubic', fill_value='extrapolate')
      eps_regridded[i, :] = fint(nB_const)
      
      d2pint = interp1d(nb_ortho[:, i], d2p_tbl[i, :], 'cubic', fill_value='extrapolate')
      d2p_tbl_regridded[i, :] = d2pint(nB_const)
    
    mu_l_tbl_regridded = np.gradient(eps_regridded, Ye[0], axis=0, edge_order=2)
    d2p_tbl_regridded = np.gradient(d2p_tbl_regridded, Ye[0], axis=0, edge_order=2)
      
    for index,nb in enumerate(nb_ortho[:, 0]):
      muint = interp1d(nB_const, mu_l_tbl_regridded[i, :], 'cubic', fill_value='extrapolate')
      d2pint = interp1d(nB_const, d2p_tbl_regridded[i, :], 'cubic', fill_value='extrapolate')
      mu_l_tbl[:,index]  = muint(nb) #np.gradient(eps_fit[:,index],Ye[0],edge_order=2)               # [m_n]
      d2p_tbl[:,index]   = d2pint(nb) #np.gradient(d2p_tbl[:,index],Ye[0],edge_order=2)
    end = timer()
    print("... done.")
    print(f"Total time spent computing derivatives: {end-start:.2f} [s]")


    ### Formatting and writing to file
    print("Formatting...") 
    lines_ = list() # List containing the formatted data to write
    start = timer()   
    for i,ye in enumerate(Ye[0]):
      first_call = 1
      for index,nb in enumerate(nb_ortho[:, i]):
        if first_call:
          ww = ent_fit[i,index]
          first_call = 0
        ent = ent_fit[i,index] - ww + 1e-14
        # if ent < 0:
        #     print("WARNING ! NEGATIVE ENTHALPY AFTER RESCALING")
        lines_.append("{0}\t{1:.16e}\t{2:.16e}\t{3:.16e}\t{4:.16e}\t{5:.16e}\t{6:.16e}\t{7:.16e}\t{8:.16e}\t{9:.16e}".format(index,nb,ener_fit[i,index],press_fit[i,index],ent,ye,cs2_tbl[i,index],mu_l_tbl[i,index]*m0_MeV,d2p_tbl[i,index],0.))
    end = timer()
    print("... done.")
    print(f"Total time spent formatting: {end-start:.2f} [s]")
    list_names = ["density", "electron fraction", "pressure", "internal energy", "lepton chemical potential", "neutron mass", "proton mass", "starting integer for theta", "starting integer for nb", "starting integer for ye"]
    self.set_fittable_ortho({name:array for name,array in zip(list_names, (nb_ortho, Ye, press_fit/p_convert, eps_fit, mu_l_tbl, m0_MeV, m_p, int_t_0, int_nb_0, int_ye_0))})

    #Appending to file
    print("Writing data to file...")
    start = timer()
    print("{} lines to write.".format(len(lines_)))
    with open(self.get_eosdir() + f"fit/{self.get_tablename()}{tablename_suffix}.d","a") as filefit:
      filefit.writelines("\n".join(lines_))
    end = timer()
    print("... done.")
    print(f"Total time spent writing data on file: {end-start:.2f} [s]")
        
  def write_coefs(self):
    list_coefs = self.get_coefs()
    
    if self.get_nb_arg() == 1:
      coefs = self.get_coefs()
            
      # Computing GPP parameters
      gamma_low = self.get_gamma_low()
      kappa_low_cgs = self.get_kappa_low_cgs()
      kappa_low_lor = self.get_kappa_low_lor()
      n_lim2 = self.get_nB_min()
      x_lim2 = np.log(n_lim2)
      n_lim1 = self.get_nB_mid()
      x_lim1 = np.log(n_lim1)
      def compute_Gamma_K(gamkap_tuple):
        Gamma, Kappa = gamkap_tuple
        eq1 = Kappa*Gamma*(n_lim1*10)**(Gamma-1.) - kappa_low_lor*gamma_low * (n_lim1*10)**(gamma_low-1)
        eq2 = Kappa*Gamma*(n_lim2*10)**Gamma - n_lim2*(dfunc_1D(x_lim2, *(*coefs, 0., 0.)) + d2func_1D(x_lim2, *(*coefs, 0., 0.)))*m0_MeV*p_convert/c2_cgs/rhonuc_cgs
        return [eq1,eq2]
      GamKap = root(compute_Gamma_K, (gamma_low, kappa_low_lor)).x
      
      
      Gam_GPP     = GamKap[0]
      Kap_GPP_lor = GamKap[1] #lor
      Kap_GPP     = Kap_GPP_lor*lor_to_si(Gam_GPP)/cgs_to_si(Gam_GPP) #cgs
      Lambda_GPP = (1. - gamma_low/Gam_GPP)*kappa_low_cgs*(n_lim1*m0_MeV*eps_convert)**gamma_low/m0_MeV/p_convert
      d_GPP      = (gamma_low * (Gam_GPP - gamma_low)/((Gam_GPP-1.)*(gamma_low-1.)) * kappa_low_cgs*(n_lim1*m0_MeV*eps_convert)**(gamma_low-1.))/c2_cgs
      Lambda     = (Kap_GPP*(n_lim2*m0_MeV*eps_convert)**Gam_GPP - m0_MeV*p_convert*n_lim2*dfunc_1D(x_lim2, *(*coefs, 0., 0.)))/m0_MeV/p_convert + Lambda_GPP
      ddd        = Kap_GPP*(n_lim2*m0_MeV*eps_convert)**(Gam_GPP-1.)/(Gam_GPP-1.)/c2_cgs + d_GPP + (Lambda - Lambda_GPP)/n_lim2 - func_1D(x_lim2, *(*coefs, 0., 0.))

      with open("1-arg_par_eos.d",'w') as parfile:
        header = "21\tType of the EOS (cf. documentation of Eos::eos_from_file)\n"
        name   = f"Pseudo-polytropic fit of one-argument EoS. The fitted EoS is: {self.get_eosdir()[19:]}\n"
        nbcoef = f"{len(list_coefs)}\t\t\tNumber of coefficients\n"
        coefs  = "\t".join(list_coefs.astype(str)) + "\tcoefficients: first n-1 for polynomial expansion of kappa; last is alpha\n"
        limnb1  = f"{10.*n_lim1}\t\t\tlower limiting density between polytrope and GPP [0.1 fm^-3]\n"
        limnb2  = f"{10.*n_lim2}\t\t\tlower limiting density between GPP and fit [0.1 fm^-3]\n"
        kappa  = f"{self.get_kappa_low_lor()}\t\t\tpressure coefficient for low densities kappa_low [rho_nuc c^2 / n_nuc^gamma]\n"
        gamma  = f"{gamma_low}\t\t\tadiabatic index for low densities gamma_low\n"
        mass   = f"{m0_MeV}\t\t\tbaryon mass m_B [MeV]\n"
        Lbd    = f"{Lambda*m0_MeV*p_convert/rhonuc_cgs/c2_cgs}\t\t\tLambda parameter of fit [rhonuc c^2]\n"
        mu_0   = f"{ddd}\t\t\tEnergy continuity parameter d of fit [dimensionless]\n"
        Kappa_GPP = f"{Kap_GPP_lor}\t\t\tPressure coefficient of GPP [rho_nuc c^2/ n_nuc^Gamma_GPP]\n"
        Gamma_GPP = f"{Gam_GPP}\t\t\tDensity exponent of GPP\n"
        Lbd_GPP = f"{Lambda_GPP*m0_MeV*p_convert/rhonuc_cgs/c2_cgs}\t\t\tLambda parameter of GPP [rhonuc c^2]\n"
        ddd_GPP = f"{d_GPP}\t\t\tEnergy continuity parameter d of GPP [dimensionless]\n"
        
        
        parfile.write(f"{header}{name}{nbcoef}{coefs}{limnb1}{limnb2}{kappa}{gamma}{mass}{Lbd}{mu_0}{Kappa_GPP}{Gamma_GPP}{Lbd_GPP}{ddd_GPP}")
      return None
    else:
      print("write_coefs() not implemented yet for 2 and 3 arguments ! Aborting...")
      sys.exit()
