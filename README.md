How to use the fitting software for CompOSE equations of state (EoS).

[CompOSE website](http://compose.obspm.fr)

# Purpose
This file describes the steps to follow to perform the fit of any CompOSE table using the pseudo-polytrope formalism for one- and two-argument EoS.
The fit procedure is described in [**Phys. Rev. D 109, 103022 (2024)**](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.109.103022), [arXiv:2311.02653 [astro-ph.HE]](https://doi.org/10.48550/arXiv.2311.02653) but we here give a summary of the ideas. An expression of the form
```math
   \varepsilon(x) = P(x)e^{\alpha{x}},
```
where $`P`$ is a polynomial and $`x`$ the logarithm of the baryon number density, is used to fit the internal energy density in the density region that corresponds to the core. 
Then, a simplified polytropic "crust" is attached to describe the EoS down to lower densities, which is connected to the high-density fit in a thermodynamically consistent 
way thanks to a Generalized Polytrope ([Phys. Rev. D 102, 083027 (2020)](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.109.103022), [arXiv:2008.03342 [astro-ph.HE]](https://doi.org/10.48550/arXiv.2008.03342)).
For any detail about the CompOSE format, we refer to the [CompOSE manual](https://compose.obspm.fr/manual).

# Code prerequisites
This code was written using Python 3.10. Please install the appropriate version of Python3. The following libraries are needed:
* `numpy`
* `scipy`
* `matplotlib`
* This software uses the CompOSE code that can be downloaded from [the website](https://compose.obspm.fr/software).
The prerequisites for the CompOSE code are specified therein, as well as the link to the CompOSE GitLab repository.

# Installation
Install Python3 and the packages with pip3. Download and compile the CompOSE code.

# Data preparation
Here are the instruction on how to prepare the data to be used by the software.
## One-argument EoS
The argument is the baryon number density.
Download a cold EoS from CompOSE. Then, unzip the file in a dedicated folder. The
equation of state should be stored in a directory of the form `path_to_data/<eos_name>/`.

## Two-argument EoS
Download a General Purpose EoS from CompOSE. Then, unzip the file in a dedicated folder. The
equation of state should be stored in a directory of the form `path_to_data/<eos_name>/`.
Create a "fit" directory inside that folder. There, you must copy the eos.yq, eos.nb and eos.micro (if present) files. 
Then, in the "fit" directory, create an eos.t file which comprises three lines, one column, the entries must either be " 0", " 0" and " 0.0000000000000000"
or " 1", " 1" and the value of the first temperature entry. The eos.parameters file that will be created must be consistent with this choice.
Copy the compose executable to the "fit" directory.
You must create the eos.quantities and eos.parameters files with the compose executable by typing "1" and "2", respectively, when prompted,
and then following the instructions. A possible set of inputs to give is the following:
  * When creating eos.quantities, choose 2 regular thermodynamic quantities, and choose pressure + energy density (indices 1 and 24). Please always check the indices as the CompOSE sofwtare might get updates and new indices.
  * The second set of quantities can be skipped (choose 0). You can also skip the error estimates (idem).
  * When creating eos.parameters, you must first choose the interpolation orders for the arguments (1, 2, or 3). You are free
    to chose what you like. Then, ensure you type 1 to ask for $\beta$ equilibrium computation, to be done after the fit has been performed.
  * Type 1 to ask for a loop to tabulate the arguments. Then, type the minimum and maximum values that come from eos.nb and eos.yq,
    as well as the number of grid points and the scaling type (0 = linear, else = logarithmic). It is advised to use logarithmic
    for the density and linear for the electron fraction.

# Running the code
In the input.json file, you must choose the fitting input parameters:
* `nb_arg`: the number of EoS arguments.
* `tablename`: the name you want to give to your table (default is `eos_fit`).
* `path`: the path to the EoS folder.
* `eos_name`: the name of the folder containing the EoS. It is concatenated with the latter to form the full path to the EoS directory.
* The density interval you want to use by specifying `nB_min` and `nB_max`, the minimum and maximum density.
* `nB_mid` is used to provide the low bound at which the GPP starts. The GPP ends at `nB_min`, where the fit starts.
* `gamma_low`: the polytropic index to be set for the low-density polytrope.
* `kappa_low`: the pressure coefficient to be set for the low-density polytrope if the number of arguments of the EoS is 1. Not applicable if the number of EoS arguments is chosen to be 2.
* `dom_coef_Ye`: the proportionality coefficient $`A`$ of the low-density polytrope pressure coefficient if the number of arguments of the EoS is 2 (see Eq. (25) and the following paragraph in the paper).
Not applicable if the number of EoS arguments is chosen to be 1.
* `nb_coef`: the number of coefficients of the polynomial $`P`$. Remember that a degree 6 polynomial has 7 coefficients.

Once all the parameters have been chosen, run the code by typing:

`python3 run_fit_eos.py`

We give below some more details and recommendations on the input parameters for the cases of EoSs with one and two arguments.

# Input parameters recommendations

## One-argument EoS
For the density interval, we recommend using `nB_min = 8e-2` [fm$`^{-3}`$] and `nB_max = 1` [fm$`^{-3}`$] as a starting point.
For the number of coefficients, we recommend to start with 3 to fit with a quadratic polynomial.
The steps of the code are the following:

* The table is read
* The fit is performed according to the input parameters.
* The data is written to a standard Lorene format. It can be found under the name `tablename`.d in the directory where the EoS is stored. The columns of the standard Lorene format is n° of line, baryon number density in [fm$`^{-3}`$], energy density in [g$`\cdot`$cm$`^{-3}`$], pressure in [dyn$\cdot$cm$`^{-2}`$]

## Two-argument EoS
The two arguments must be the density of baryons and the electron fraction.

In the input.json file, you must then choose the fitting input parameters: the number of EoS arguments (supposed to be 2), the density 
interval you want to use (encoded in `nB_min`, `nB_max`). Once again `nB_min = 8e-2` [fm$`^{-3}`$] and `nB_max = 1` [fm$`^{-3}`$] are good values to start with.
Please provide the path to the directory where your EoS is stored and the name of the EoS, as well as a name for your fitted table ("eos_fit" is default).
The coefficients `dom_coef_Ye` is chosen to be a multiplicative coefficient to a polynomial fit to $`\varepsilon(`$`nB_min`$`, Y_e)`$ (see Eq. (25) and the following
paragraph in the paper). The initial guess is a default one inside the code. The steps of the code are the following:

* The table is read
* The fit is performed according to the input parameters.
* The fit coefficients are used to compute the fitted table, which is written to the extended 2D Lorene format [doc to be written] used in numerical
   simulation, as well as in the CompOSE format. The name of the output tables are fit/"tablename".d and fit/eos.thermo. This table has an orthogonal grid in $`(n_B, Y_e)`$.
* An *orthogonalisation* procedure is performed. The result is a table which has an orthogonal $`(H, Y_e)`$ grid.
* The $\beta$ equilibrium is computed by running the compose code. If the eos.parameters and eos.quantities files were created correctly, the program should compute the $\beta$-equilibrated table. If not, the simplest debug procedure is to check that the eos.parameters file corresponds to the actual argument values. Do not forget to check the CompOSE manual for details. The output is an eos.beta file that contains the $`p(n_B)`$, $`e(n_B)`$, $`Y_e(n_B)`$ profiles at $\beta$ equilibrium.
* The last step is to reformat the data computed in the previous one. Two output files can now be found as fit/"tablename"_beta.d
  and fit/ye_beta.d


## Three-argument EoS
***To be implemented***

# Running tests

Two example input files are given for the [one-argument RG(SLy4) EoS](https://compose.obspm.fr/eos/134) and the [two-argument RG(SLy4) EoS](https://compose.obspm.fr/eos/123). Note that
in their default state they assume that the EoS is stored in a directory called SLy4 or RGSLy4, respectively, and that the code is run from the parent directory. The example files should be moved to input.json to be read by the code.

# Reporting issues
Neutrino production rates are still to be implemented.

If you find a bug, please write to gael.servignat[at]gmail.com.
Feel free to send any suggestions, they are very welcome.