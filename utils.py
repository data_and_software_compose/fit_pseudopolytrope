import numpy as np
from scipy.optimize import curve_fit, least_squares, root
from timeit import default_timer as timer
from param import *

def find_closest(array: np.array, value: float):
  idx = (np.abs(array-value)).argmin()
  return idx

def cm_to_inch(d):
	return d/2.54

def read_1D_table_compose(eosdir):
  print("Reading 1D CompOSE table ...")
  start = timer()
  
  #Extracting n_B
  n_B = np.loadtxt(eosdir + "eos.nb",skiprows=2)
  N_n = len(n_B)
  
  eps   = np.zeros(N_n)
  press = np.zeros(N_n)
  mu_q  = np.zeros(N_n)
  mu_l  = np.zeros(N_n)
  mu_B  = np.zeros(N_n)
  free  = np.zeros(N_n)
  
  #Retrieving the neutron and proton mass + thermo quantities
  with open(eosdir+"eos.thermo","r") as thermofile:
    mass=np.array(thermofile.readline().split()).astype(np.float64)
    m_n = mass[0] # mass of neutron in MeV
    m_p = mass[1] # mass of proton  in MeV
    # Extracting thermo quantities.
    compteur = 0
    while compteur < N_n:
      line = np.array(thermofile.readline().split())
      ints,thermo = line[:3],line[3:]
      ints   = ints.astype(int)
      thermo = thermo.astype(np.float64) 
      if compteur==0:
          int_nb_0 = ints[1]
      eps[ints[1]-int_nb_0]   = thermo[6]                       # e/(m_n*n_B) - 1 [dimensionless]
      press[ints[1]-int_nb_0] = thermo[0]*n_B[ints[1]-int_nb_0] # p               [MeV fm^-3]
      mu_q[ints[1]-int_nb_0]  = thermo[3]                       # mu_q/m_n        [dimensionless]
      mu_l[ints[1]-int_nb_0]  = thermo[4]                       # mu_l/m_n        [dimensionless]
      mu_B[ints[1]-int_nb_0]  = thermo[2]                       # mu_B/m_n - 1    [dimensionless]
      free[ints[1]-int_nb_0]  = thermo[5]                       # f/(m_n*n_B) - 1 [dimensionless]
          
      compteur += 1
  
  end = timer()
  print("... done.")
  print(f"Time spent reading the 1D CompOSE table: {end-start:.2f} [s]")
  return m_n,m_p,n_B,eps,press,free,mu_B,mu_q,mu_l

def read_2D_table_compose(eosdir):
  print("Reading 2D CompOSE table...")
  start = timer()
  #Extracting Y_e and n_B arrays as given in the table
  Y_e = np.loadtxt(eosdir + "eos.yq",skiprows=2)
  n_B = np.loadtxt(eosdir + "eos.nb",skiprows=2)
  #Extracting lowest temperature of table
  T = np.loadtxt(eosdir + "eos.t")[2] #MeV
  print(f"The first temperature occurrence of the table is T = {T} [MeV]")
  
  N_n = len(n_B)
  N_Y = len(Y_e)
  nbpts   = N_n*N_Y
  eps     = np.zeros((N_n,N_Y))
  press   = np.zeros((N_n,N_Y))
  mu_q    = np.zeros((N_n,N_Y))
  mu_l    = np.zeros((N_n,N_Y))
  mu_B    = np.zeros((N_n,N_Y))
  free    = np.zeros((N_n,N_Y))
  entropy = np.zeros((N_n,N_Y))
  
  #Retrieving the neutron and proton mass + thermo quantities
  with open(eosdir + "eos.thermo","r") as thermofile:
    mass=np.array(thermofile.readline().split()).astype(np.float64)
    m_n = mass[0] # mass of neutron in MeV
    m_p = mass[1] # mass of proton  in MeV
    
    # Extracting thermo quantities. Rows: function of Y_e. Columns: function of n_B
    compteur = 0
    while compteur < nbpts:
      line = np.array(thermofile.readline().split())
      ints,thermo = line[:3],line[3:]
      ints   = ints.astype(int)
      thermo = thermo.astype(np.float64) 
      if compteur==0:
        int_t_0  = ints[0]
        int_nb_0 = ints[1]
        int_ye_0 = ints[2]
      if ints[0]-int_t_0==0:
        eps[ints[1]-int_nb_0,ints[2]-int_ye_0]     = thermo[6]                       # e/(m_n*n_B) - 1 [dimensionless]
        press[ints[1]-int_nb_0,ints[2]-int_ye_0]   = thermo[0]*n_B[ints[1]-int_nb_0] # p               [MeV fm^-3]
        entropy[ints[1]-int_nb_0,ints[2]-int_ye_0] = thermo[1]                       # s/n_B           [dimensionless]
        mu_q[ints[1]-int_nb_0,ints[2]-int_ye_0]    = thermo[3]                       # mu_q/m_n        [dimensionless]
        mu_l[ints[1]-int_nb_0,ints[2]-int_ye_0]    = thermo[4]                       # mu_l/m_n        [dimensionless]
        mu_B[ints[1]-int_nb_0,ints[2]-int_ye_0]    = thermo[2]                       # mu_B/m_n - 1    [dimensionless]
        free[ints[1]-int_nb_0,ints[2]-int_ye_0]    = thermo[5]                       # f/(m_n*n_B) - 1 [dimensionless]
        
        compteur += 1
  
  end = timer()
  print("... done.")
  print(f"Time spent reading the 2D table: {end-start:.2f} [s]")
  return m_n,m_p,Y_e,n_B,eps,press,free,mu_B,mu_q,mu_l,entropy,T,int_nb_0,int_t_0,int_ye_0




### Fit utilities ###
def func_1D(x, *coefs):    # NOTE x=log(n)
  Lambda     = coefs[-1]
  d          = coefs[-2]
  g          = coefs[-3]
  coefs_poly = coefs[:-3]
  poly = np.poly1d(np.flip(coefs_poly))
  ftry = poly(x)*np.exp(g*x) + d - Lambda*np.exp(-x)   #  Fit function 
  return ftry

def dfunc_1D(x, *coefs):
  Lambda     = coefs[-1]
  d          = coefs[-2]
  g          = coefs[-3]
  coefs_poly = coefs[:-3]
  poly = np.poly1d(np.flip(coefs_poly))
  dpoly = np.polyder(poly)
  dftry = (g*poly(x) + dpoly(x))*np.exp(g*x) + Lambda*np.exp(-x) # First derivative (w.r.t. x=log(n))
  return dftry

def d2func_1D(x, *coefs):
  Lambda     = coefs[-1]
  d          = coefs[-2]
  g          = coefs[-3]
  coefs_poly = coefs[:-3]
  poly = np.poly1d(np.flip(coefs_poly))
  dpoly = np.polyder(poly)
  d2poly = np.polyder(dpoly)
  d2ftry = (g*g*poly(x) + 2.*g*dpoly(x) + d2poly(x))*np.exp(g*x) - Lambda*np.exp(-x) # Second derivative (w.r.t x=log(n))
  return d2ftry

def func_eps(x, *coefs):
  g          = coefs[-1]
  coefs_poly = coefs[:-1]
  poly = np.poly1d(np.flip(coefs_poly))
  return poly(x)*np.exp(g*x)

def func_2D(X, poly, ggg):
  x,y = X
  list_poly = []
  for p in poly:
      list_poly.append(p(y))
  g = ggg(y)
  ftry = np.sum([list_poly[i]*x**i for i in range(len(poly))], axis = 0)*np.exp(g*x)
  return ftry

def dfunc_2D(X, poly, ggg):    # NOTE x=log(n),y=Y_e
  x,y = X
  
  N_poly = len(poly)
  pp = np.poly1d([1.]*N_poly)
  pp_der = np.polyder(pp)
  
  list_poly = []
  list_polyderx = []
  list_polydery = []
  for index,p in enumerate(poly):
    list_poly.append(p(y))
    if index < N_poly-1:
      list_polyderx.append(pp_der[index]*poly[index+1](y))
    list_polydery.append(np.polyder(p)(y))
  g    = ggg(y)
  dyg  = np.polyder(ggg)(y)

  dxftry = (np.sum([list_polyderx[i]*x**i for i in range(N_poly-1)], axis = 0) + g * np.sum([list_poly[i]*x**i for i in range(N_poly)], axis = 0))*np.exp(g*x)
  dyftry = (np.sum([list_polydery[i]*x**i for i in range(N_poly)], axis = 0) + x*dyg * np.sum([list_poly[i]*x**i for i in range(N_poly)], axis = 0))*np.exp(g*x)
  return dxftry, dyftry

def write_compose_format(eosdir, n_B, Ye, press, eps, mu_l, m_n, m_p, int_nb_0, int_t_0, int_ye_0): #This function has to be extended to 3 arguments
  with open(eosdir + "eos.thermo","w") as filethermo:
    filethermo.write(f"{m_n}\t{m_p}\t{int(1)}\n")
    for i,ye in enumerate(Ye):
      for j,nb in enumerate(n_B):
        line = "{0}\t{1}\t{2}\t{3:.16e}\t{4:.16e}\t{5:.16e}\t{6:.16e}\t{7:.16e}\t{8:.16e}\t{9:.16e}\t{10}\n".format(int(0) + int_t_0,j + int_nb_0,i + int_ye_0,press[i,j]/(nb*p_convert),0.,0.,0.,mu_l[i,j],eps[i,j],eps[i,j],int(0))
        filethermo.write(line)
  return None

def compute_GPP_param(table_fit, *coefs, verbose = False):
  gamma_low = table_fit.get_gamma_low()
  kappa_low_cgs = table_fit.get_kappa_low_cgs()
  kappa_low_lor = table_fit.get_kappa_low_lor()
  n_lim2 = table_fit.get_nB_min()
  x_lim2 = np.log(n_lim2)
  n_lim1 = table_fit.get_nB_mid()
  x_lim1 = np.log(n_lim1)
  
  def compute_Gamma_K(gamkap_tuple):
    Gamma, Kappa = gamkap_tuple
    eq1 = Kappa*Gamma*(n_lim1*10)**(Gamma-1.) - kappa_low_lor*gamma_low * (n_lim1*10)**(gamma_low-1)
    eq2 = Kappa*Gamma*(n_lim2*10)**Gamma - n_lim2*(dfunc_1D(x_lim2, *(*coefs, 0., 0.)) + d2func_1D(x_lim2, *(*coefs, 0., 0.)))*m0_MeV*p_convert/c2_cgs/rhonuc_cgs
    return [eq1,eq2]
  GamKap = root(compute_Gamma_K, (gamma_low, kappa_low_lor)).x
  
  
  Gam_GPP     = GamKap[0]
  Kap_GPP_lor = GamKap[1] #lor
  Kap_GPP     = Kap_GPP_lor*lor_to_si(Gam_GPP)/cgs_to_si(Gam_GPP) #cgs
  if verbose == True:
    res = compute_Gamma_K(GamKap)
    ratio1 = res[0]/(Kap_GPP_lor*Gam_GPP*(10.*n_lim1)**(Gam_GPP-1.))
    ratio2 = res[1]/(Kap_GPP_lor*Gam_GPP*(10.*n_lim2)**Gam_GPP)
    if abs(ratio1) > 1e-5 or abs(ratio2) > 1e-5:
      print( "==========================================================================")
      print( "WARNING !! Continuity of Gamma_1 at transition is likely not fulfilled.   ")
      print(f"The relative differences are {ratio1} and {ratio2}")
      print( "               Try changing the external polytrope...                     ")
      print( "==========================================================================")
  Lambda_GPP = (kappa_low_cgs*(n_lim1*m0_MeV*eps_convert)**gamma_low - Kap_GPP*(n_lim1*m0_MeV*eps_convert)**Gam_GPP)/m0_MeV/p_convert
  d_GPP      = (gamma_low * (Gam_GPP - gamma_low)/((Gam_GPP-1.)*(gamma_low-1.)) * kappa_low_cgs*(n_lim1*m0_MeV*eps_convert)**(gamma_low-1.))/c2_cgs
  Lambda     = (Kap_GPP*(n_lim2*m0_MeV*eps_convert)**Gam_GPP - m0_MeV*p_convert*n_lim2*dfunc_1D(x_lim2, *(*coefs, 0., 0.)))/m0_MeV/p_convert + Lambda_GPP
  ddd        = Kap_GPP*(n_lim2*m0_MeV*eps_convert)**(Gam_GPP-1.)/(Gam_GPP-1.)/c2_cgs + d_GPP + (Lambda - Lambda_GPP)/n_lim2 - func_1D(x_lim2, *(*coefs, 0., 0.))
  
  return Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda

def residuals(coefs, *args):
  # Unpacking
  g = coefs[-1]
  coefs_poly = coefs[:-1]
  table_fit, nb_fit, eps_fit, pressdivn_fit, Gamma1_fit = args

  
  
  # Computing GPP parameters
  Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda = compute_GPP_param(table_fit, *coefs)
  
  # Computing residuals
  sum_res = np.array([])
  len_dens = np.log(nb_fit[-1])-np.log(nb_fit[0])
  for index,nb in enumerate(nb_fit):
    xx = np.log(nb)
    if index==len(nb_fit)-1:
      delta_xx = np.log(nb) - np.log(nb_fit[index-1])
    else:
      delta_xx = np.log(nb_fit[index+1]) - np.log(nb)

    eps_ref = eps_fit[index]
    eps     = func_1D(xx, *(*coefs_poly, g, ddd, Lambda))
    sum_res = np.append(sum_res, (eps_ref - eps)*np.exp(-xx)*delta_xx/len_dens/eps_ref)

    pressdivn_ref = pressdivn_fit[index]
    pressdivn     = dfunc_1D(xx, *(*coefs_poly, g, ddd, Lambda))
    sum_res = np.append(sum_res, (np.log(pressdivn_ref) - np.log(pressdivn))*delta_xx/len_dens/np.log(pressdivn_ref))

    Gam1_ref = Gamma1_fit[index]
    Gam1     = 1. + d2func_1D(xx, *(*coefs_poly, g, ddd, Lambda))/dfunc_1D(xx, *(*coefs_poly, g, ddd, Lambda))
    sum_res = np.append(sum_res, (Gam1_ref-Gam1)*delta_xx/len_dens/Gam1_ref)
  return sum_res

def diff_press(nbt_p, dFdx_p, *coefs):
  return (nbt_p*dfunc_1D(np.log(nbt_p), *coefs) - nbt_p*dFdx_p)/nbt_p*dFdx_p

def fit_GPP(nbt, Ft, gam1, dFdx, educated_guess, method, table_fit, bounds):
  opti = least_squares(residuals, educated_guess, method = method, ftol = 1e-12, args = (table_fit, nbt, Ft, dFdx, gam1), jac = '2-point', bounds = bounds)
  print("Fit result:")
  # print(opti)
  popt = opti.x  # Best fit coefficients  
  print(popt)
  
  # Computing GPP parameters
  Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda = compute_GPP_param(table_fit, *popt, verbose = True)
  
  return popt, Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda

def fit_func_1D(nbt, Ft, gam1, dFdx, initial_guess, method, table_fit, bounds_guess_ini = (-np.inf, np.inf)):
  p0 = initial_guess
  lognbt = np.log(nbt)
  educated_guess, pcov = curve_fit(func_eps, lognbt, Ft, p0, bounds = bounds_guess_ini)
  print("First guess: ",educated_guess) #,'\n',"Covariance :\n", pcov)
  
  if table_fit.get_nb_arg() == 1:
    width_bounds = 1e-2
  elif table_fit.get_nb_arg() == 2:
    width_bounds = 1e-4
  
  bound_inf = educated_guess - width_bounds*np.abs(educated_guess)
  bound_sup = educated_guess + width_bounds*np.abs(educated_guess)
  if method == 'trf':
    bounds = (bound_inf, bound_sup)
  elif method == 'lm':
    bounds = (-np.inf, np.inf)
  
  popt, Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda = fit_GPP(nbt, Ft, gam1, dFdx, educated_guess, method, table_fit, bounds)

  return popt

def fit_func_2D(n_B, Y_e, eps, gam1, dFdx, initial_guess, table_fit, method):
  N_Y = len(Y_e)
  N_n = len(n_B)
  dimfitNY = N_Y*N_n
  print(f"Fitting on {N_n} grid points for n_B, {N_Y} grid points for Y_e, for a total of {dimfitNY} points")
  lognbt = np.log(n_B)
  
  p0 = initial_guess
  nb_coef1D = len(p0)
  tmp_coefs = np.zeros((N_Y, nb_coef1D+2))
  GPP_coefs = np.zeros((N_Y, 4))
  Kappa_Ye = table_fit.get_coefs_Ye()
  AAA      = table_fit.get_dom_coef_Ye()
  
  
  for i in range(N_Y):
    table_fit.set_kappa_low(AAA*Kappa_Ye(Y_e[i]))
    if i==0:
      bounds_guess_ini = (-np.inf, np.inf)
    else:
      bounds_guess_ini = (tmp_coefs[i-1, :-2] - 1e-4*np.abs(tmp_coefs[i-1, :-2]), tmp_coefs[i-1, :-2] + 1e-4*np.abs(tmp_coefs[i-1, :-2]))
    popt_tmp = fit_func_1D(n_B, eps[i, :], gam1[i, :], dFdx[i, :], initial_guess, method, table_fit, bounds_guess_ini)
    print(f"Kappa: {table_fit.get_kappa_low()}")
    # Computing GPP parameters
    Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP, ddd, Lambda = compute_GPP_param(table_fit, *popt_tmp, verbose = True)

    initial_guess = popt_tmp
    tmp_coefs[i, :] = np.append(popt_tmp, [ddd, Lambda])
    GPP_coefs[i, :] = np.array([Gam_GPP, Kap_GPP, d_GPP, Lambda_GPP])
    p0 = popt_tmp

  # table_fit.set_kappa_low(kappa_ini)
  return tmp_coefs, GPP_coefs

def read_1D_table_beta(eosdir):
  start = timer()
  print("Reading 1D beta-equilibrated table...")
  data_ = np.loadtxt(eosdir + "eos.beta")
  
  n_B   = data_[:,0]
  Y_e   = data_[:,1]
  ener  = data_[:,2]
  press = data_[:,3]
  
  end = timer()
  print("... done.")
  print(f"Time spent reading the 1D beta-equilibrated table: {end-start:.2f} [s]")
  return n_B,Y_e,ener,press


def read_2D_table_lorene(eosdir):
  start = timer()
  print("Reading 2D table in extended Lorene format")
  with open(eosdir+"fit/eos_fit.d",'r') as filelorene:
    for i in range(5):
      filelorene.readline()
    
    array_nbpts = filelorene.readline().split()
    nbpts_dens, nbpts_ye = int(array_nbpts[0]), int(array_nbpts[1])
    
    for i in range(3):
      filelorene.readline()
    
    nB    = np.zeros((nbpts_dens, nbpts_ye))
    ener  = np.zeros((nbpts_dens, nbpts_ye))
    press = np.zeros((nbpts_dens, nbpts_ye))
    entha = np.zeros((nbpts_dens, nbpts_ye))
    Ye    = np.zeros((nbpts_dens, nbpts_ye))
    cs2   = np.zeros((nbpts_dens, nbpts_ye))
    mue   = np.zeros((nbpts_dens, nbpts_ye))
    d2p   = np.zeros((nbpts_dens, nbpts_ye))
    
    for j in range(nbpts_ye):
      for i in range(nbpts_dens):
        data_ = np.array(filelorene.readline().split()).astype(np.float64)
        nB[i, j]    = data_[1]
        ener[i, j]  = data_[2]
        press[i, j] = data_[3]
        entha[i, j] = data_[4]
        Ye[i, j]    = data_[5]
        cs2[i, j]   = data_[6]
        mue[i, j]   = data_[7]
        d2p[i, j]   = data_[8]
  
  end = timer()
  print("... done.")
  print(f"Time spent reading the 2D Lorene table: {end-start:.2f} [s]")
  return nB, ener, press, entha, Ye, cs2, mue, d2p

def regulafalsi(f,a,b,ref,IterMax,Tol,*args):
  k = 0
  c = (b*f(a, *args) - a*f(b, *args))/(f(a, *args) - f(b, *args))
  while abs(f(c, *args)/ref)>Tol and k<IterMax:
    c = (b*f(a, *args) - a*f(b, *args))/(f(a, *args) - f(b, *args))
    if f(a, *args)*f(c, *args)<0:
      b=c
    else:
      a=c
    k+=1
  return c