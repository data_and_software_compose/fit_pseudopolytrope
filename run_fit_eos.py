from fit import Fit_EoS
from os.path import isfile
import sys
import json


def main():
  # Reading the input parameters
  with open('input.json', 'r') as file_input:
    input_params = json.load(file_input)
  nb_arg = input_params['nb_arg']
  eosdir = f"{input_params['path']}{input_params['eos_name']}/"

  # Creating fit object
  table_fit = Fit_EoS(nb_arg, input_params['tablename'], eosdir, input_params['nB_mid'],
                      input_params['nB_min'], input_params['nB_max'], input_params['gamma_low'],
                      input_params['kappa_low'], input_params['dom_coef_Ye'])

  # When using nb_arg=2, checking the existence of necessary files to compute beta equilibrium
  if nb_arg == 2:
    if not isfile(table_fit.get_eosdir() + "fit/eos.quantities"):
      if not isfile(table_fit.get_eosdir() + "fit/eos.parameters"):
        print("Files eos.parameters and eos.quantities missing.")
        print("Please create eos.parameters and eos.quantities file !")
        print("See CompOSE code manual on the website. Aborting...")
        sys.exit()
      print("File eos.quantities missing.")
      print("Please create eos.quantities file.")
      print("See CompOSE code manual on the website. Aborting...")
      sys.exit()
    else:
      if not isfile(table_fit.get_eosdir() + "fit/eos.parameters"):
        print("File eos.parameters missing.")
        print("Please create eos.parameters file.")
        print("See CompOSE code manual on the website. Aborting...")
        sys.exit()

  # Reading the table
  table_fit.read_table()

  # Performing the fit
  table_fit.perform_fit(input_params['nb_coef'])

  # Outputs
  if nb_arg == 1:
    press_cgs, ener_cgs = table_fit.compute_thermo_1D()
    header = f"# EoS created by pseudo-polytropic fit of CompOSE \
               {input_params['eos_name']} EoS. Low-density EoS is a polytrope."
    table_fit.write_lorene_1D(input_params['tablename'], header,
                              table_fit.get_dens()[
      table_fit.get_dens() < table_fit.get_nB_max()],
      ener_cgs, press_cgs)
    table_fit.write_coefs()
  elif nb_arg == 2:
    # Writing the physical quantities to the 2D output files
    table_fit.write_lorene_2D()
    table_fit.write_compose()
    table_fit.orthogonalise_table()

    # Computing beta equilibrium
    table_fit.compute_beta_eq()

    # Writing the 1D output files related to beta-equilibrium
    table_fit.write_beta_eq_files()

  print("End of fitting procedure")
  return 0


if __name__ == "__main__":
  main()
