from numpy import pi

### Physical constants and conversion factors ## 1e-39 g = 1.6e-29 MeV/c^2
c_si = 2.99792458E+8 	 #< Velocity of light [m/s]
rho_nuc = 1.6605390666e17 #kg/m^3
n_nuc = 0.1e45 #m^-3
g_si    = 6.6743E-11    #SI
msol_si = 1.9885E+30    #kg
msol_geom = g_si*msol_si/(c_si*c_si)
mev_si = 1.602176565E-13   #< One MeV [J]
m0 = rho_nuc/n_nuc #kg
m0_MeV = m0*c_si*c_si/mev_si
k_B = 8.61733034E-11     #< Boltzmann constant [MeV/K]
fm3_to_cm3 = 1e-39

rhonuc_cgs = rho_nuc * 1e-3
c2_cgs = c_si * c_si * 1e4
p_convert = mev_si * 1.e45 * 10. # Conversion from MeV/fm^3 to cgs (dyn/cm^2)
eps_convert = mev_si * 1.e42 / (c_si*c_si) # From MeV/fm^3 to g/cm^3
m_e = 0.510998950 #MeV
hbarc = 197.3269788 #MeV fm
g_to_kg = 1e-3 #kg/g
cm_to_m = 1e-2 #m/cm

#Conversion factors for kappa coef
lor_to_si  = lambda gamma: rho_nuc*c_si*c_si/(n_nuc*m0)**gamma
si_to_geom = lambda gamma: (g_si/(c_si*c_si))**(1-gamma)/(c_si*c_si)
cgs_to_si  = lambda gamma: (g_to_kg)**(1.-gamma)*(cm_to_m)**(3.*gamma-1.)
###